# cuttlexyz workshop

Workshopleader: ...

A web-based design tool for digital cutting machines.
Generate custom SVGs in seconds with templates, or use the editor to make your own designs.

[web site](https://cuttle.xyz/)

[insta](https://www.instagram.com/cuttlexyz/)