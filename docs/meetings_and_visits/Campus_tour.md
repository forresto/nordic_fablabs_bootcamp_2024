# campus tour

- visit to [3d print lab](https://www.aalto.fi/en/workshops-aalto-school-of-arts-design-and-architecture/3d-print)

Impressive lab that showed us ceramic printing

- Visit to [Chemarts](https://chemarts.aalto.fi/)

![Alt text](../images/chemart_tour.jpeg)

Interesting work and we also saw a interesing book that is avalible online. 
[2nd edition of The CHEMARTS Cookbook](https://chemarts.aalto.fi/index.php/news/2nd-edition-of-the-chemarts-cookbook-available/)


- Visit to the [glassblowing lab](https://www.aalto.fi/en/workshops-aalto-school-of-arts-design-and-architecture/glass-workshop)

