# annual meeting

## 

## Annual meeting of Nordic Fab Labs Network 
*FabLab Aalto 09:00–12:00 UTC +2, 2024-01-18* 
Zoom code: 

Participants: 


1. Opening the meeting
2. Selecting chairman, secretary, two protocol inspectors and if necessary, two vote counters
3. Legality and quorum of the meeting are noted
4. Agenda is accepted
5. The financial statements, the annual report and the statement of function inspectors / auditors are presented
6. Counting of legal voters
7. Deciding on the adoption of the annual accounts and the discharge to the Board of Directors and other accountable persons
8. An action plan, an estimate of revenue and expenditure, and the amount of membership, joining fee and membership fee shall be established
9. the Chairman and other members of the Board of Directors are elected.
The chairmanship will rotate between nordic countries each year and will rotate in the following order. In the board should be at least one representative from each country:
The vice chairman will be the chairman the year after

- Finland 
- Iceland 
- Denmark 
- Sweden 
- Norway 
- Faroe Islands 
- Greenland 

If there is no member from that country that should have chairmanship that year, the next country in the line will substitute the chairmanship
10. One or two function auditors and one deputy auditor, or one or two auditors and deputy auditors are voted to revise the meeting notes from the previous annual meeting.
11. Changing rules and unpacking the organization

## pitching of the next bootcamp

1. Vestmannaeyjar Frosti - It's a nice place
2. Denmark - Lars - it's also a nice place - [slides](../files/Bootcamp_pitch_dk_lars_compressed.pdf)