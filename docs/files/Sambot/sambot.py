import serial
import time

# Command characters from mods
# X axis
#X->f (forwards)
#x->r (backwards)
# Y axis
#Y->f
#y->r
# Z axis
#Z->f
#z->r

# Make sure to set to correct ports before running
xcom = 'COM3'
ycom = 'COM5'
zcom = 'COM6'

# Save commands to cmds.txt or modify name accordingly
f = open("cmds.txt", "r")

# Timeout set 0.064 based on calculations from Janis motor control boards code
timeout = 0.064

# Set to True once correct COMS are selected
XYZcomms = False

if XYZcomms:
    x = serial.Serial(xcom, 115200)
    y = serial.Serial(ycom, 115200)
    z = serial.Serial(zcom, 115200)

while 1:
    char = f.read(1)

    # Stop once file is done          
    if not char: 
        break

    if XYZcomms:
        if char == 'X':
            #print("X: Forward")
            x.write('f'.encode())
        elif char == 'x':
            #print("X: Backwards")
            x.write('r'.encode())
    
        elif char == 'Y':
            #print("Y: Forward")
            y.write('f'.encode())
        elif char == 'y':
            #print("Y: Backwards")
            y.write('r'.encode())
    
        elif char == 'Z':
            #print("Z: Forward")
            z.write('f'.encode())
        elif char == 'z':
            #print("Z: Backwards")
            z.write('r'.encode())
        
        time.sleep(timeout)

f.close()