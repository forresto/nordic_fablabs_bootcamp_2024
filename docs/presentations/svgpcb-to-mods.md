# SvgPcb to Mods 

![SvgPcb to Mods Modal](../images/download_mods_modal.jpg)

Kris finalized the SvgPcb-to-Mods feature during the Nordic Bootcamp. The feature is not merged yet, but it will be available in a few weeks from now via the [official SvgPcb](https://github.com/leomcelroy/svg-pcb) site.

For now, there is a [docs page](https://github.com/kr15h/svg-pcb/blob/feat_svgToMods/DOCS/DOWNLOAD_MODS.md) and a [video](https://youtu.be/8KzZo6WPrYQ) available.
