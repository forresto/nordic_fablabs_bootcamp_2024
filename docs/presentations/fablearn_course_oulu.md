# Fab Learning Academy

Fab Learning Academy is a Global Certification program for educators using digital fabrication technologies in K-12 settings. Both formal and informal educators, pre-service teachers, and administrators are invited to participate. This curriculum focuses on three domains to support educators in technological tools, innovative practices, and the science of learning.

[Information about the program.](https://fla.academany.org/)

presentor: Iván Sánchez Milara

[Slides](../files/FabLearningAcademy_presentation.pdf)
